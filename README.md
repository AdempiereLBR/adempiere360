# README #

This project contains a copy of Adempiere Project with some modifications to be compatible with Java 7 and 8.

If you are planning to use Localization Brazil (v201601 or below), you must use the tag Adempiere306LTS (Java 6) or Adempiere360LTS.Kenos2015 (Java 7 or 8).

If you do not intend to use Localization Brazil you can use the newer versions -> https://github.com/adempiere/adempiere